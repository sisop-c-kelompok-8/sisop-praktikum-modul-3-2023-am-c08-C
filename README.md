# Sisop Praktikum Modul 3 2023 Am C08 C
# PRAKTIKUM 3 KELOMPOK 8 SISOP C


### Praktikum Sisop Modul 3
Group Members:
1. Keysa Anadea (5025211028)
2. Anneu Tsabita (5025211026)
3. Dimas Pujangga (5025211212)

## Question 1       
Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan Algoritma Huffman untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut! 
**Catatan :**       
- Pada encoding ASCII, setiap karakter diwakili oleh 8 bit atau 1 byte, yang cukup untuk merepresentasikan 256 karakter yang berbeda, contoh: 
     -  Huruf A	: 01000001
     -  Huruf a	: 01100001
- Untuk karakter selain huruf tidak masuk ke perhitungan jumlah bit dan tidak perlu dihitung frekuensi kemunculannya
- Agar lebih mudah, ubah semua huruf kecil ke huruf kapital

**Jawab**   

```c
struct MinHeapNode *newNode(char data, unsigned freq) {
  struct MinHeapNode *temp = (struct MinHeapNode *)malloc(sizeof(struct MinHeapNode));

  temp->left = temp->right = NULL;
  temp->data = data;
  temp->freq = freq;

  return temp;
}

struct MinHeap *createMinH(unsigned capacity) {
  struct MinHeap *minHeap = (struct MinHeap *)malloc(sizeof(struct MinHeap));

  minHeap->size = 0;

  minHeap->capacity = capacity;

  minHeap->array = (struct MinHeapNode **)malloc(minHeap->capacity * sizeof(struct MinHeapNode *));
  return minHeap;
}
```
`newNode`: Fungsi ini digunakan untuk membuat dan menginisialisasi node baru dalam pohon Huffman. Node ini memiliki data karakter, frekuensi kemunculan, serta pointer ke node anak kiri dan kanan.

`createMinH`: Fungsi ini digunakan untuk membuat dan menginisialisasi min heap yang akan digunakan dalam algoritma Huffman. Min heap ini akan menyimpan pointer ke node-node dalam pohon Huffman.

```c
void swapMinHeapNode(struct MinHeapNode **a, struct MinHeapNode **b) {
  struct MinHeapNode *t = *a;
  *a = *b;
  *b = t;
}
```
`swapMinHeapNode`: Fungsi ini digunakan untuk menukar posisi dua node dalam min heap.
```c
void minHeapify(struct MinHeap *minHeap, int idx) {
  int smallest = idx;
  int left = 2 * idx + 1;
  int right = 2 * idx + 2;

  if (left < minHeap->size && minHeap->array[left]->freq < minHeap->array[smallest]->freq)
    smallest = left;

  if (right < minHeap->size && minHeap->array[right]->freq < minHeap->array[smallest]->freq)
    smallest = right;

  if (smallest != idx) {
    swapMinHeapNode(&minHeap->array[smallest], &minHeap->array[idx]);
    minHeapify(minHeap, smallest);
  }
}
```
`minHeapify`: Fungsi ini digunakan untuk memperbaiki min heap jika ada node yang melanggar aturan min heap setelah operasi penambahan atau penghapusan node.

```c
int checkSizeOne(struct MinHeap *minHeap) {
  return (minHeap->size == 1);
}
```
`checkSizeOne`: Fungsi ini digunakan untuk memeriksa apakah ukuran min heap hanya satu.
```c
struct MinHeapNode *extractMin(struct MinHeap *minHeap) {
  struct MinHeapNode *temp = minHeap->array[0];
  minHeap->array[0] = minHeap->array[minHeap->size - 1];

  --minHeap->size;
  minHeapify(minHeap, 0);

  return temp;
}
```
`extractMin`: Fungsi ini digunakan untuk mengeluarkan node dengan frekuensi terendah dari min heap
```c
void insertMinHeap(struct MinHeap *minHeap, struct MinHeapNode *minHeapNode) {
  ++minHeap->size;
  int i = minHeap->size - 1;

  while (i && minHeapNode->freq < minHeap->array[(i - 1) / 2]->freq) {
    minHeap->array[i] = minHeap->array[(i - 1) / 2];
    i = (i - 1) / 2;
  }
  minHeap->array[i] = minHeapNode;
}
```
insertMinHeap`: Fungsi ini digunakan untuk menyisipkan node baru ke dalam min heap dan memastikan aturan min heap tetap terjaga.
`

```c
void buildMinHeap(struct MinHeap *minHeap) {
  int n = minHeap->size - 1;
  int i;

  for (i = (n - 1) / 2; i >= 0; --i)
    minHeapify(minHeap, i);
}
```
`buildMinHeap`: Fungsi ini digunakan untuk membangun min heap dari array node-node yang ada.

```c
int isLeaf(struct MinHeapNode *root) {
  return !(root->left) && !(root->right);
}
```
`isLeaf`: Fungsi ini digunakan untuk memeriksa apakah sebuah node adalah daun dalam pohon Huffman.

```c
struct MinHeap *createAndBuildMinHeap(char data[], int freq[], int size) {
  struct MinHeap *minHeap = createMinH(size);

  for (int i = 0; i < size; ++i)
    minHeap->array[i] = newNode(data[i], freq[i]);

  minHeap->size = size;
  buildMinHeap(minHeap);

  return minHeap;
}
```
`createAndBuildMinHeap`: Fungsi ini digunakan untuk membuat dan membangun min heap berdasarkan data karakter dan frekuensi kemunculannya.

```c
struct MinHeapNode *buildHuffmanTree(char data[], int freq[], int size) {
  struct MinHeapNode *left, *right, *top;
  struct MinHeap *minHeap = createAndBuildMinHeap(data, freq, size);

  while (!checkSizeOne(minHeap)) {
    left = extractMin(minHeap);
    right = extractMin(minHeap);

    top = newNode('$', left->freq + right->freq);

    top->left = left;
    top->right = right;

    insertMinHeap(minHeap, top);
  }
  return extractMin(minHeap);
}
```
`buildHuffmanTree`: Fungsi ini digunakan untuk membangun pohon Huffman berdasarkan data karakter dan frekuensi kemunculannya.

```c
void printArray(int arr[], int n, char c) {
    FILE *fp = fopen("temp.txt", "a");

    if (fp == NULL) {
        printf("Error opening file\n");
        return;
    }

    fprintf(fp, "%c", c);
    for (int i = 0; i < n; ++i) {
        fprintf(fp, "%d", arr[i]);
    }
    fprintf(fp, "%c", '\n');

    fclose(fp);
}
```
`printArray`: Fungsi ini digunakan untuk mencetak kode Huffman untuk karakter tertentu ke dalam file temp.txt.

```c
void printHCodes(struct MinHeapNode *root, int arr[], int top) {
  if (root->left) {
    arr[top] = 0;
    printHCodes(root->left, arr, top + 1);
  }
  if (root->right) {
    arr[top] = 1;
    printHCodes(root->right, arr, top + 1);
  }
  if (isLeaf(root) && strchr(send, root->data)!=NULL) {
    printArray(arr, top, root->data);
  }
}
```
`printHCodes`: Fungsi ini digunakan untuk mencetak semua kode Huffman dalam pohon Huffman dan menyimpannya ke dalam file temp.txt.

```c
void HuffmanCodes(char data[], int freq[], int size) {
  struct MinHeapNode *root = buildHuffmanTree(data, freq, size);

  int arr[50], top = 0;

  printHCodes(root, arr, top);
}
```
`HuffmanCodes`: Fungsi ini merupakan fungsi utama untuk melakukan kompresi menggunakan algoritma Huffman. Fungsi ini membangun pohon Huffman, mencetak kode Huffman, dan menyimpannya ke dalam file temp.txt.

```c
int count_line(char filename[]){
    int count = 0;
    char line[512];
    FILE *fp = fopen(filename, "r");

    if (fp == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    while (fgets(line, 512, fp) != NULL) {
        count++;
    }

    fclose(fp);

    return count;
}
```
`count_line`: Fungsi ini digunakan untuk menghitung jumlah baris dalam sebuah file.


```c
int findLength(char c){
FILE *fp = fopen("temp.txt", "r");
if (fp == NULL) {
    printf("Error opening file.\n");
    return 1;
}
char line[512];
while (fgets(line, 512, fp) != NULL) {
    if (line[0] == c) {
        fclose(fp);
        return strlen(line) - 1;
    }
}

fclose(fp);
return -1;
}
```
`findLength`: Fungsi ini digunakan untuk mencari panjang kode Huffman untuk karakter tertentu dalam file temp.txt.
```c

int translate(char filename[]) {
    FILE *fp = fopen(filename, "a");
    FILE *fs = fopen("temp.txt", "r");
    FILE *fc = fopen("file.txt", "r");

if (fp == NULL || fs == NULL || fc == NULL) {
    printf("Error opening file.\n");
    return 1;
}

char line[512];
char c;
int count = 0;
while ((c = fgetc(fc)) != EOF) {
    fseek(fs, 0, SEEK_SET);
    c = toupper(c);
    int found = 0; // Flag to indicate if character c is found
    while (fgets(line, 512, fs) != NULL) {
        if (line[0] == c) {
            found = 1;
            break;
        }
    }
    if (found) {
        count += strlen(line) - 2;
        fwrite(line + 1, strlen(line) - 2, 1, fp);
    }
}

fclose(fp);
fclose(fs);
fclose(fc);
return count;
}
```
`translate`: Fungsi ini digunakan untuk melakukan translasi karakter dalam file input file.txt menjadi kode Huffman yang sesuai, dan menyimpan hasil translasi ke dalam file output translated.txt.

```c
int main()
{
    int parent_to_child_pipe[2];
    int child_to_parent_pipe[2];

    if (pipe(parent_to_child_pipe) == -1 || pipe(child_to_parent_pipe) == -1)
    {
        fprintf(stderr, "Pipe failed\n");
        return 1;
    }

    pid_t pid = fork();
    if (pid == 0)
    {
        // Child process
        close(parent_to_child_pipe[1]);
        close(child_to_parent_pipe[0]);

        // Read data from parent
        char arr[26];
        int arrint[26];
        char sent[26];
        read(parent_to_child_pipe[0], arr, sizeof(arr));
        read(parent_to_child_pipe[0], arrint, sizeof(arrint));
        read(parent_to_child_pipe[0], sent, sizeof(sent));
        close(parent_to_child_pipe[0]);

        // Copy 'sent' to 'send'
        strcpy(send, sent);

        // Perform Huffman encoding
        HuffmanCodes(arr, arrint, sizeof(arr));

        // Write data to parent
        int countLine = count_line("temp.txt");
        write(child_to_parent_pipe[1], &countLine, sizeof(countLine));
        FILE *fp = fopen("temp.txt", "r");
        if (fp == NULL)
        {
            perror("Failed to open file");
            exit(1);
        }
        char line[512];
        while (fgets(line, 512, fp) != NULL)
        {
            write(child_to_parent_pipe[1], &line, sizeof(line));
        }
        fclose(fp);
        close(child_to_parent_pipe[1]);
        exit(0);
    }
    else if (pid > 0)
    {
        // Parent process
        close(parent_to_child_pipe[0]);
        close(child_to_parent_pipe[1]);

        FILE *fp;
        char filename[100];
        char c;

        fp = fopen("file.txt", "r");
        if (fp == NULL)
        {
            printf("Error opening file\n");
            return 1;
        }

        int freq[26] = {0};
        char alpha[26] = {'\0'};
        int idx = 0;
        while ((c = fgetc(fp)) != EOF)
        {
            if (isalpha(c))
            {
                c = toupper(c);
                int pos = c - 'A';
                freq[pos]++;
                if (freq[pos] == 1)
                {
                    alpha[idx] = c;
                    idx++;
                }
            }
        }

        char arr[26];
        int arrfr[26];

        int j = 0;
        for (int i = 0; i < 26; i++)
        {
            alpha[i] = 'A' + i;
            arrfr[i] = freq[i];

            if (freq[i] != 0)
            {
                send[j] = alpha[i];
                j++;
            }
        }

        for (int i = 0; i < 26; i++)
        {
            arr[i] = alpha[i];
        }

        fclose(fp);

        write(parent_to_child_pipe[1], arr, sizeof(arr));
        write(parent_to_child_pipe[1], arrfr, sizeof(arrfr));
        write(parent_to_child_pipe[1], send, sizeof(send));
        close(parent_to_child_pipe[1]);

        int n;
        read(child_to_parent_pipe[0], &n, sizeof(int));

        char coded[n][512];
        for (int i = 0; i < n; i++)
        {
            read(child_to_parent_pipe[0], coded[i], sizeof(char) * 512);
        }
               close(child_to_parent_pipe[0]);

        int number = 0;
        for (int i = 0; i < 26; i++)
        {
            int calc;
            if (freq[i] != 0)
            {
                calc = freq[i] * 8;
                number += calc;
            }
        }

        printf("Jumlah bit sebelum dilakukan kompresi adalah %d\n", number);

        number = translate("translated.txt");

        printf("Jumlah bit sesudah dilakukan kompresi adalah %d\n", number);

        system("rm temp.txt");
    }
    else
    {
        // Fork failed
        fprintf(stderr, "Fork failed\n");
        return 1;
    }

    return 0;
}

```
`main`: Fungsi utama program. Fungsi ini menggunakan dua pipe dan fork untuk mengatur komunikasi antara parent process dan child process. Pada child process, dilakukan kompresi menggunakan algoritma Huffman dan hasilnya dikirim kembali ke parent process. Pada parent process, dilakukan baca tulis data serta perhitungan dan tampilan jumlah bit sebelum dan setelah kompresi.


#### Output Question 1

![1a](/uploads/fd239f6b93b858d1f7cdb1e5ad98f108/1a.jpg)
![1b](/uploads/24934fd1a2026270e8eef4b6a52d61b7/1b.jpg)


## Question 2
Fajar sedang sad karena tidak lulus dalam mata kuliah Aljabar Linear. Selain itu, dia tambah sedih lagi karena bertemu matematika di kuliah Sistem Operasi seperti kalian 🥲. Karena dia tidak bisa ngoding dan tidak bisa menghitung, jadi Fajar memohon jokian kalian. Tugas Fajar adalah sebagai berikut.    
#### 2A
**_SOAL :_**     
Membuat program C dengan nama kalian.c, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.   
**_Pengaplikasian Pada Program:_**      
![Screenshot_2023-05-13_190527](/uploads/97e724b9d984a1b744659d9426db4606/Screenshot_2023-05-13_190527.png)
![Screenshot_2023-05-13_190623](/uploads/5f08eead9f65d6f9334cf5a7bbe1ba6e/Screenshot_2023-05-13_190623.png)         
**_Penjelasan Kode:_**      
Pada tahap pertama, program menggunakan fungsi srand() dan rand() untuk menginisialisasi nilai acak di dalam matriks. Penggunaan fungsi random untuk menghasilkan nilai acak antara 1 dan 5 untuk elemen matriks pertama dan nilai acak antara 1 dan 4 untuk elemen matriks kedua. Matriks pertama diinisialisasi dengan ordo 4x2, dan matriks kedua dengan ordo 2x5. Program menggunakan nested loop untuk melakukan inisialisasi matriks, dimana loop luar digunakan untuk mengiterasi baris dan loop dalam digunakan untuk mengiterasi kolom.        

Pada tahap kedua, program menggunakan fungsi shmget() untuk membuat shared memory yang akan menampung hasil perkalian matriks. Shared memory ini menggunakan key yang bernilai 1234 dan ukuran yang sama dengan ukuran matriks hasil perkalian, yaitu 4x5. Jika proses pembuatan shared memory berhasil, maka program akan mengembalikan sebuah integer yang merepresentasikan id dari shared memory tersebut.      

Pada tahap terakhir, program melakukan perkalian matriks antara matriks pertama dan kedua menggunakan nested loop yang sama seperti pada tahap pertama. Hasil perkalian disimpan di dalam shared memory dengan menggunakan pointer ke shared memory tersebut dan operator *. Setelah perkalian selesai dilakukan, program akan mengakses dan menampilkan isi dari shared memory yang berisi hasil perkalian matriks.    

#### 2B
**_SOAL :_**     
Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. 
(Catatan: wajib menerapkan konsep shared memory)    
**_Pengaplikasian Pada Program:_**     
![Screenshot_2023-05-13_190956](/uploads/38e492fe993da8e9c55dc8a8d65b06b7/Screenshot_2023-05-13_190956.png) 
**_Penjelasan Kode:_**   
Pertama, program cinta.c membuat variabel shmid dan menginisialisasi variabel key dengan nilai yang sama dengan nilai yang digunakan pada program kalian.c. Kemudian, program cinta.c mencoba untuk mengakses shared memory yang sudah dibuat dengan menggunakan shmget() dan memasukkan hasilnya ke dalam variabel shmid. Jika gagal dalam mengakses shared memory, maka program akan menampilkan pesan kesalahan dengan perror() dan keluar dari program dengan exit(1).

Setelah berhasil mengakses shared memory, program cinta.c meng-attach shared memory tersebut menggunakan shmat(). Pointer hasil dari shmat() kemudian disimpan ke dalam variabel shmarr.

Selanjutnya, program cinta.c menampilkan hasil perkalian matriks yang terdapat pada shared memory dengan menggunakan pointer (*shmarr)[i][j] pada looping for yang terakhir. Hasil perkalian matriks ditampilkan ke layar dengan format yang sama seperti program kalian.c.

#### 2C
**_SOAL :_**        
Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.
Contoh: 
array [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], ...],

maka:

1 2 6 24 120 720 ... ... …
(Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial)      
**_Pengaplikasian Pada Program:_**   
![Screenshot_2023-05-13_191150](/uploads/ad9057e6ea4272669a1c014b20a2ccd3/Screenshot_2023-05-13_191150.png)   
![Screenshot_2023-05-13_191312](/uploads/884399584fea39d2434dbb0da3bfbd31/Screenshot_2023-05-13_191312.png)
![Screenshot_2023-05-13_191351](/uploads/b9017d7d9ccc0a6d0ee567362f394fc4/Screenshot_2023-05-13_191351.png)   
**_Penjelasan Kode:_**   
Pertama, program melakukan meng-attach shared memory yang diperoleh dari program lain dengan menggunakan fungsi shmget() dan shmat(). Shared memory tersebut berisi matriks dengan ukuran 4x5 yang berisi angka-angka untuk dihitung faktorialnya. Kemudian, program menampilkan isi matriks tersebut ke layar.

Setelah itu, program membuat 4 thread untuk melakukan perhitungan faktorial. Tiap thread akan menghitung faktorial dari setiap elemen pada baris matriks tertentu. Fungsi faktorial() dipanggil oleh tiap thread untuk menghitung faktorial dari tiap elemen. Hasil perhitungan faktorial disimpan pada array hasil_faktorial.

Setelah semua thread selesai melakukan perhitungan, program menampilkan hasil perhitungan faktorial ke layar. Terakhir, program menghitung waktu CPU yang digunakan selama menjalankan program dan menampilkan nilai tersebut ke layar. 

#### 2D
**_SOAL :_**        
Buatlah program C ketiga dengan nama sisop.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. Dokumentasikan dan sampaikan saat demo dan laporan resmi.      
**_Pengaplikasian Pada Program:_**     
![Screenshot_2023-05-13_191521](/uploads/544557750e443edbf19c38379fbafd08/Screenshot_2023-05-13_191521.png)
![Screenshot_2023-05-13_191618](/uploads/d590a60521d99004dd6cc052269dfea3/Screenshot_2023-05-13_191618.png)

**_Penjelasan Kode:_**   
Pertama-tama, program akan meng-attach shared memory yang dihasilkan oleh program "kalian.c" menggunakan fungsi shmget() dan shmat(). Setelah itu, program akan menampilkan matriks hasil perkalian dari program "kalian.c".

Selanjutnya, program akan melakukan penghitungan faktorial dari masing-masing elemen pada setiap baris matriks. Penghitungan faktorial dilakukan dalam fungsi faktorial() dengan menggunakan parameter n yang menunjukkan baris yang akan dihitung faktorialnya. Pada setiap elemen pada baris tersebut, dilakukan perulangan sebanyak n kali dan hasil perkalian dari perulangan tersebut akan disimpan pada array hasil_faktorial.

Setelah semua perhitungan faktorial selesai dilakukan, program akan menampilkan hasil perhitungan faktorial dengan menggunakan perulangan untuk menampilkan setiap elemen pada array hasil_faktorial.

Program ini juga mencatat waktu eksekusi program dengan menggunakan fungsi clock() dan menghitung selisih waktu awal dan akhir eksekusi. Waktu eksekusi program akan ditampilkan pada akhir program dengan menggunakan variabel cpu_time. Akhirnya, program akan melepaskan shared memory dengan menggunakan fungsi shmdt().

#### Source Code Question 2
**kalian.c:**  
![Screenshot_2023-05-13_190527](/uploads/97e724b9d984a1b744659d9426db4606/Screenshot_2023-05-13_190527.png)
![Screenshot_2023-05-13_190623](/uploads/5f08eead9f65d6f9334cf5a7bbe1ba6e/Screenshot_2023-05-13_190623.png) 

**cinta.c:**     
![Screenshot_2023-05-13_191150](/uploads/ad9057e6ea4272669a1c014b20a2ccd3/Screenshot_2023-05-13_191150.png)   
![Screenshot_2023-05-13_191312](/uploads/884399584fea39d2434dbb0da3bfbd31/Screenshot_2023-05-13_191312.png)
![Screenshot_2023-05-13_191351](/uploads/b9017d7d9ccc0a6d0ee567362f394fc4/Screenshot_2023-05-13_191351.png)


**sisop.c:**        
![Screenshot_2023-05-13_191521](/uploads/544557750e443edbf19c38379fbafd08/Screenshot_2023-05-13_191521.png)
![Screenshot_2023-05-13_191618](/uploads/d590a60521d99004dd6cc052269dfea3/Screenshot_2023-05-13_191618.png)

#### Output Question 2
**kalian.c:**           
![Screenshot_2023-05-13_195143](/uploads/8d2cd9ab564f96e881c260e5f4231d3a/Screenshot_2023-05-13_195143.png)

**cinta.c**
![Screenshot_2023-05-13_195239](/uploads/be73946cd1898d662f894afaddf4666d/Screenshot_2023-05-13_195239.png)

**sisop.c**
![Screenshot_2023-05-13_195337](/uploads/c4b135035ca2da045c82d2a74d123f0d/Screenshot_2023-05-13_195337.png)

### KESIMPULAN
Pada percobaan dalam kode cinta.c dan sisop.c dapat disimpulkan bahwa kode yang menggunakan thread lebih lama dalam hal waktu eksekusi karena terdapat overhead yang diperlukan untuk menginisialisasi thread dan memindahkan data di antara thread yang berbeda.

Dalam kasus kode ini, menggunakan thread untuk menghitung faktorial masing-masing baris matriks mengharuskan program untuk membuat thread baru untuk setiap baris. Proses ini memakan waktu karena harus mengalokasikan memori dan membuat struktur data untuk setiap thread, serta memindahkan data yang diperlukan ke dalam thread.

Sementara pada kode tanpa menggunakan thread, perhitungan faktorial dilakukan secara berurutan pada setiap baris matriks tanpa overhead tambahan dari thread, sehingga waktu eksekusinya lebih cepat.

Namun, penggunaan thread dapat menjadi lebih efisien dalam situasi di mana ada banyak tugas yang dapat dijalankan secara bersamaan dan membutuhkan waktu yang lama untuk menyelesaikan tugas tersebut. Dalam kasus seperti itu, menggunakan thread dapat memungkinkan program untuk mengeksekusi tugas-tugas tersebut secara bersamaan dan mengurangi waktu keseluruhan yang dibutuhkan untuk menyelesaikan semua tugas.

## Question 3
Elshe saat ini ingin membangun usaha sistem untuk melakukan stream lagu. Namun, Elshe tidak paham harus mulai dari mana.
Catatan: 
- Untuk mengerjakan soal ini dapat menggunakan contoh implementasi message queue pada modul.
- Perintah DECRYPT akan melakukan decrypt/decode/konversi dengan metode sebagai berikut:
1. ROT13
    ROT13 atau rotate 13 merupakan metode enkripsi sederhana untuk melakukan enkripsi pada tiap   karakter di string dengan menggesernya sebanyak 13 karakter.
    Contoh:
2. Base64
    Base64 adalah sistem encoding dari data biner menjadi teks yang menjamin tidak terjadinya modifikasi yang akan merubah datanya selama proses transportasi. Tools Decode/Encode.
3. Hex
    Hexadecimal string merupakan kombinasi bilangan hexadesimal (0-9 dan A-F) yang merepresentasikan suatu string. Tools.
  
#### Source Code Question 3
- Source code user.c

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

//struct untuk message buffer
struct msg_buf{

    int id;
    long msg_type;
    char msg_text[500];
};

int main(){

    struct msg_buf data;
    int msg_id;

    //melakukan inisialisasi bahwa id message queue yaitu 12
    key_t key;
    key = 12;

    //membuat msg_id
    msg_id = msgget(key, 0666|IPC_CREAT);

    if(msg_id == -1){
        puts("Error in creating queue\n");
        exit(0);
    }  

    //handle jika inputan user belum selesai
    int running = 1;
    char buffer_msg[200];

    while(running){

        puts("Masukkan perintah yang sesuai dengan format: ");
        puts("* Untuk menambahkan lagu, ketik \"ADD Judul_lagu\"");
        puts("* Untuk play lagu, ketik \"PLAY \"Judul_Lagu\"\"");
        puts("* Untuk decrypt maka ketik \"DECRYPT\"");
        puts("* Untuk memunculkan daftar lagu, ketik \"LIST\"");
        puts("* Untuk selesai, maka ketik \"end\"");

        //memasukkan inputan ke buffer_msg
        fgets(buffer_msg, 200, stdin);


        //cek apakah stdin berupa perintah DECRYPT, LIST, PLAY, ataupun ADD
        if(strstr(buffer_msg, "DECRYPT") || strstr(buffer_msg, "LIST") || strstr(buffer_msg, "PLAY") || strstr(buffer_msg, "ADD")){
            data.msg_type = 1;

            data.id = getpid();

            strcpy(data.msg_text, buffer_msg);

            if(msgsnd(msg_id, (void*) &data, 500, 0) == -1) puts("Message not sent");
        }

        else if(strstr(buffer_msg, "end")){
            data.id = getpid();
            data.msg_type = 1;

            strcpy(data.msg_text, "end");
            msgsnd(msg_id, (void*) &data, 500, 0);

            exit(EXIT_FAILURE);
        }

        else{
            data.id = getpid();
            data.msg_type = 1;
            
            strcpy(data.msg_text, "UNKNOWN COMMAND");
            msgsnd(msg_id, (void*) &data, 500, 0);
        }

    }

    return 0;
}


- Source code stream.c


#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <semaphore.h>
#include <fcntl.h>
#include <stdint.h>
#include <ctype.h>

#define SHM_KEY 1234 // Shared memory key
#define SEM_NAME "/my_semaphore" // Nama semaphore

//struct untuk pid_shared_t
typedef struct {
    pid_t pid1;
    pid_t pid2;
} pid_shared_t;

char* hexToASCII(const char* hex) {
    size_t len = strlen(hex);
    if (len % 2 != 0) return NULL; // Return NULL jika jumlah digit hex bukan genap

    size_t output_len = len / 2;
    char* output = malloc(output_len + 1);
    if (output == NULL) return NULL; // Return NULL jika alokasi memori gagal

    for (size_t i = 0; i < output_len; i++) {
        char byte[3] = {hex[i * 2], hex[i * 2 + 1], '\0'}; // Ambil 2 digit hex untuk setiap byte ASCII
        output[i] = (char)strtol(byte, NULL, 16); // Convert hex menjadi bilangan desimal
    }
    output[output_len] = '\0'; // Tambahkan null terminator pada akhir string ASCII

    return output;
}

static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};
 
void build_decoding_table() {
 
    decoding_table = malloc(256);
 
    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char) encoding_table[i]] = i;
}

unsigned char *base64(const char *data,
                             size_t input_length,
                             size_t *output_length) {
 
    if (decoding_table == NULL) build_decoding_table();
 
    if (input_length % 4 != 0) return NULL;
 
    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;
 
    unsigned char *decoded_data = malloc(*output_length);
    unsigned char *hasil_decode = malloc(*output_length);

    if (decoded_data == NULL) return NULL;
 
    for (int i = 0, j = 0; i < input_length;) {
 
        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
 
        uint32_t triple = (sextet_a << 3 * 6)
        + (sextet_b << 2 * 6)
        + (sextet_c << 1 * 6)
        + (sextet_d << 0 * 6);
 
        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    for (int i = 0; i < *output_length; i++){

        if(decoded_data[i] != 0 && decoded_data[i] != 127){
            hasil_decode[i] = decoded_data[i];
        }
        else break;
    }
 
    return hasil_decode;
}
 
char *rot13(char *src){

    if(src == NULL) return NULL;

    char* result = malloc(strlen(src) + 1);

    if(result != NULL){
        strcpy(result, src);

        char* current_char = result;

        while(*current_char != '\0'){

            if((*current_char >= 97 && *current_char <= 122) || (*current_char >= 65 && *current_char <= 90)){

                if(*current_char > 109 || (*current_char > 77 && *current_char < 91)){

                    *current_char -= 13;
                }
                else{
                    *current_char += 13;
                }
            }
            current_char++;
        }
    }

    return result;
}

void DECRYPT(){

    char* json_data;
    char *filename = "song-playlist.json";
    FILE *file = fopen(filename, "r");

    if(file == NULL){
        puts("Gagal membuka file");
        exit(1);
    }

    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    rewind(file);

    json_data = (char*) malloc(file_size);

    fread(json_data, file_size, 1, file);

    fclose(file);

    char *json_start = strchr(json_data, '[');
    char *json_end = strchr(json_data, ']');

    char *method_start = strstr(json_start, "\"method\"");

    FILE *fp = fopen("playlist.txt", "w");

    while(method_start && method_start < json_end) {

        // Mengambil nilai method
        char *method_value_start = strchr(method_start, ':');
        char *method_value_end = strchr(method_value_start, ',');
        int method_value_len = method_value_end - method_value_start - 3;

        char method[method_value_len + 1];
        strncpy(method, method_value_start + 3, method_value_len);
        method[method_value_len] = '\0';

        strtok(method, "\"");

        printf("Method: %s\n", method);

        // Mengambil nilai song
        char *song_start = strstr(method_value_end, "\"song\"");
        char *song_value_start = strchr(song_start, ':');
        char *song_value_end = strchr(song_value_start, '}');
        long song_value_len = song_value_end - song_value_start - 3;

        char song[song_value_len + 1];
        strncpy(song, song_value_start + 3, song_value_len);
        song[song_value_len] = '\0';

        strtok(song, "\"");

        printf("Song: %s\n", song);
        
        //mulai decrypt:
        if(strcmp(method, "rot13") == 0){
            puts("INI ROT13");

            char *result = rot13(song);
            printf("Hasil decrypt dari rot13: %s\n",result);

            fprintf(fp, "%s\n", result);
        } 

        if(strcmp(method, "base64") == 0){

            char* hasil_decode = base64(song, song_value_len, &song_value_len);

            printf("Hasil decrypt dari base64: %s\n", hasil_decode);

            fprintf(fp, "%s\n", hasil_decode);
        } 

        if(strcmp(method, "hex") == 0){

            char* hexString = hexToASCII(song);

            printf("Hasil hex to string: %s\n", hexString);

            fprintf(fp, "%s\n", hexString);

        } 

        // Pindah ke objek method dan song berikutnya
        method_start = strstr(song_value_end, "\"method\"");
    }

    fclose(fp);
}

// Fungsi untuk membandingkan dua string
int compare(const void* a, const void* b) {
    char* str1 = *(char**)a;
    char* str2 = *(char**)b;

    return strcmp(str1, str2);
}

void LIST() {

    FILE *file_song = fopen("playlist.txt", "r");
    if (file_song == NULL) {
        puts("File tidak ditemukan!");
    }

    char line[105];
    char lines[1005][105];
    char* uppercaseLines[1005];
    char* lowercaseLines[1005];
    char* numberLines[1005];
    int numUppercaseLines = 0;
    int numLowercaseLines = 0;
    int numNumberLines = 0;

    // Baca setiap baris dan cek huruf pertamanya
    for(int i = 0; i < 1005 && fgets(line, 105, file_song); i++) {
        
        //cek huruf besar
        if (line[0] >= 65 && line[0] <= 90) {
            // Alokasikan memori dan salin isi baris ke array untuk huruf besar
            uppercaseLines[numUppercaseLines] = lines[i];
            strcpy(uppercaseLines[numUppercaseLines], line);
            numUppercaseLines++;
        } 
        //cek huruf kecil
        else if (line[0] >= 97 && line[0] <= 122) {
            // Alokasikan memori dan salin isi baris ke array untuk huruf kecil
            lowercaseLines[numLowercaseLines] = lines[i];
            strcpy(lowercaseLines[numLowercaseLines], line);
            numLowercaseLines++;
        } 
        //cek angka dsb
        else{
            // Alokasikan memori dan salin isi baris ke array untuk angka
            numberLines[numNumberLines] = lines[i];
            strcpy(numberLines[numNumberLines], line);
            numNumberLines++;
        }
    }

    FILE *sort_file = fopen("sortingSong.txt", "w");

    qsort(uppercaseLines, numUppercaseLines, sizeof(char*), compare);
    qsort(lowercaseLines, numLowercaseLines, sizeof(char*), compare);
    qsort(numberLines, numNumberLines, sizeof(char*), compare);

    for(int i = 0; i < numNumberLines; i++){
        fputs(numberLines[i], sort_file);
        printf("%s\n", numberLines[i]);
    }

    for(int i = 0; i < numLowercaseLines; i++){
        fputs(lowercaseLines[i], sort_file);
        printf("%s\n", lowercaseLines[i]);
    }

    for(int i = 0; i < numUppercaseLines; i++){
        fputs(uppercaseLines[i], sort_file);
        printf("%s\n", uppercaseLines[i]);
    }

    fclose(sort_file);

}

void PLAY(const int x, const char* words){

    FILE * cek_song;
    char line_cek[150];
    char matchedLines[100][150];
    long numMatched = 0;

    cek_song = fopen("playlist.txt", "r");

    while(fgets(line_cek, 150, cek_song)){

        char* tmp = strdup(line_cek);

        for(int i = 0; i < strlen(tmp); i++){
            tmp[i] = tolower(tmp[i]);
        }

        char* wordLower = strdup(words);

        for(int i = 0; i < strlen(wordLower); i++){
            wordLower[i] = tolower(wordLower[i]);
        }

        if(strstr(tmp, wordLower)){
            
            strcpy(matchedLines[numMatched], line_cek);
            numMatched++;
        }
    }

    fclose(cek_song);

    if(numMatched == 1) printf("USER %d PLAYING \"%s\"\n", x, matchedLines[0]);
    else if(numMatched > 1){
        printf("THERE ARE %ld SONG CONTAINING \"%s\":", numMatched, words);
        puts("");
        
        for(long i = 0; i < numMatched; i++){
            printf("%ld. %s", i + 1, matchedLines[i]);
        }
    }
    else printf("THERE IS NO SONG CONTAINING \"%s\"\n", words);

}

void ADD(const int x, const char* newSong){

    FILE *add_song;
    add_song = fopen("playlist.txt", "r");

    char line[150];
    int flag = 0;
    while(fgets(line, 150, add_song)){

        if(strstr(line, newSong)){
            flag = 1;
            break;
        }
    }

    fclose(add_song);

    if(flag == 0){
        add_song = fopen("playlist.txt", "a");

        fputs(newSong, add_song);

        fclose(add_song);

        printf("USER %d ADD %s\n", x, newSong);
    }

    else puts("SONG ALREADY ON PLAYLIST");
}

struct msg_buf{

    int id;
    long msg_type;

    //menggunakan BUFSIZ untuk menentukan ukuran buffer yang akan digunakan untuk menyimpan pesan yang akan dikirimkan melalui msg queue. 
    // Ukuran buffer akan disesuaikan dengan ukuran buffer yang tersedia di sistem sehingga program dapat bekerja secara optimal
    char msg_text[BUFSIZ];

};

int main(){

    // Inisialisasi shared memory
    int shem_id = shmget(SHM_KEY, sizeof(pid_shared_t), IPC_CREAT | 0666);
    if (shem_id == -1) {
        perror("shmget");
        exit(EXIT_FAILURE);
    }

    pid_shared_t *pid_data = shmat(shem_id, NULL, 0);
    if (pid_data == (void *) -1) {
        perror("shmat");
        exit(EXIT_FAILURE);
    }

    // Inisialisasi semaphore
    sem_t semaphore;
    if (sem_init(&semaphore, 1, 1) == -1) {
        perror("sem_init");
        exit(EXIT_FAILURE);
    }

    int running = 1;
    struct msg_buf data;

    long int msg_rcv = 0;

    key_t key = 12;

    int msg_id;
    msg_id = msgget(key, 0666|IPC_CREAT);

    int flag = 0;

    while(running){

        // sem_destroy(&semaphore);
        // shmdt(pid_data);
        // shmctl(shem_id, IPC_RMID, NULL);

        msgrcv(msg_id, (void*) &data, BUFSIZ, msg_rcv, 0);

        // Akses shared memory dan semaphore
        sem_wait(&semaphore); // Mengurangi nilai semaphore menjadi 0

        if ((pid_data->pid1 == 0 && data.id != pid_data->pid2) || data.id == pid_data->pid1) {
            pid_data->pid1 = data.id;

            if(strncmp(data.msg_text, "end", 3) == 0){
                printf("Process with PID %d is end.\n", data.id);

                printf("Data received: %s\n", data.msg_text);

                pid_data->pid1 = 0;

                flag = 0;

                sem_post(&semaphore); // Meningkatkan nilai semaphore kembali menjadi 1

            }
            
            else{
                printf("Process with PID %d is running.\n", data.id);
                flag = 1;
                sem_post(&semaphore);
            } 
        } 

        else if ((pid_data->pid2 == 0 && data.id != pid_data->pid1) || data.id == pid_data->pid2) {
            pid_data->pid2 = data.id;
            
            if(strncmp(data.msg_text, "end", 3) == 0){

                printf("Process with PID %d is end.\n", data.id);
                printf("Data received: %s\n", data.msg_text);

                pid_data->pid2 = 0;

                flag = 0;

                sem_post(&semaphore); // Meningkatkan nilai semaphore kembali menjadi 1

            }

            else {
                printf("Process with PID %d is running.\n", data.id);
                flag = 1;
                sem_post(&semaphore);
            }
        } 

        else{

            printf("Process with PID %d is unacceptable.\n", data.id);
            puts("STREAM SYSTEM OVERLOAD");
            flag = 0;
            sem_post(&semaphore);
        }

        sem_wait(&semaphore);

        if(flag == 1){
            
            printf("Data received: %s with id: %d\n", data.msg_text, data.id);

            if(strncmp(data.msg_text, "DECRYPT", 7) == 0){

                DECRYPT();
                puts("");
            }

            else if(strncmp(data.msg_text, "LIST", 4) == 0){

                LIST();
                puts("");
            }

            else if(strncmp(data.msg_text, "PLAY", 4) == 0){

                char *ptr1 = strchr(data.msg_text, '\"');
                char *ptr2;

                char lagu_input[200] = {0};
                int user_id = -1;

                if(ptr1 != NULL){

                    ptr1++;
                    ptr2 = strchr(ptr1, '\"');

                    if(ptr2 != NULL) strncpy(lagu_input, ptr1, ptr2 - ptr1);
                }

                PLAY(data.id, lagu_input);
            }
            

            else if(strncmp(data.msg_text, "ADD", 3) == 0){

                char *ptr1 = strstr(data.msg_text, "ADD ");
                if (ptr1 != NULL) {
                    ptr1 += 4; // maju sebanyak 4 karakter untuk melewati "CEK "
                    char *ptr2 = strchr(ptr1, '\0'); // cari akhir string

                    // menghapus whitespace di awal string
                    while (*ptr1 == ' ') ptr1++;
                    
                    // menghapus whitespace di akhir string
                    while (*(ptr2 - 1) == ' ') ptr2--;

                    // menghitung panjang kalimat yang diambil
                    size_t len = ptr2 - ptr1;

                    // menyalin kalimat ke buffer
                    char lagu_input[200];
                    strncpy(lagu_input, ptr1, len);
                    lagu_input[len] = '\0';

                    ADD(data.id, lagu_input);
                }
            }
        }
        sem_post(&semaphore);

        puts("");

    }

    // if(strncmp(data.msg_text,"end",3) == 0) running = 0;

    sem_destroy(&semaphore);
    shmdt(pid_data);
    shmctl(shem_id, IPC_RMID, NULL);
    
    msgctl(msg_id, IPC_RMID, 0);

    return 0;
}

**_Penjelasan Kode:_**   

**Menerapkan message queue**
//struct untuk message buffer
struct msg_buf{

    int id;
    long msg_type;
    char msg_text[500];
};

int main(){

    struct msg_buf data;
    int msg_id;

    //melakukan inisialisasi bahwa id message queue yaitu 12
    key_t key;
    key = 12;

    //membuat msg_id
    msg_id = msgget(key, 0666|IPC_CREAT);

    if(msg_id == -1){
        puts("Error in creating queue\n");
        exit(0);
    }  
    ...


Di dalam konteks ini, kita menyatakan keberadaan message queue dengan ID 12. Opsi IPC_CREAT digunakan untuk memastikan bahwa message queue tersebut akan dibuat jika belum ada sebelumnya. Angka 0666 pada parameter tersebut menunjukkan bahwa akses untuk write dan read di antrian tersebut dapat dilakukan. 
Beralih ke stream.C

int running = 1;
    char buffer_msg[200];

    while(running){

        puts("Masukkan perintah yang sesuai dengan format: ");
        puts("* Untuk menambahkan lagu, ketik \"ADD Judul_lagu\"");
        puts("* Untuk play lagu, ketik \"PLAY \"Judul_Lagu\"\"");
        puts("* Untuk decrypt maka ketik \"DECRYPT\"");
        puts("* Untuk memunculkan daftar lagu, ketik \"LIST\"");
        puts("* Untuk selesai, maka ketik \"end\"");

        //memasukkan inputan ke buffer_msg
        fgets(buffer_msg, 200, stdin);


        //cek apakah stdin berupa perintah DECRYPT, LIST, PLAY, ataupun ADD
        if(strstr(buffer_msg, "DECRYPT") || strstr(buffer_msg, "LIST") || strstr(buffer_msg, "PLAY") || strstr(buffer_msg, "ADD")){
            data.msg_type = 1;
            
            //mendapatkan nilai getpid() untuk membedakan user 1 dengan user lainnya yang sedang akses file user.c. Karena, kita hanya membatasi hingga 2 user saja. 
            data.id = getpid();

            //copy buffer_msg ke struct data.msg_txt
            strcpy(data.msg_text, buffer_msg);
            
            //lalu struct data dikirim ke message queue
            if(msgsnd(msg_id, (void*) &data, 500, 0) == -1) puts("Message not sent");
        }

        else if(strstr(buffer_msg, "end")){

            //mendapatkan pid agar kita tau mana process id yang telah berhenti
            data.id = getpid();
            data.msg_type = 1;

            strcpy(data.msg_text, "end");
            msgsnd(msg_id, (void*) &data, 500, 0);

            exit(EXIT_FAILURE);
        }

        else{
            data.id = getpid();
            data.msg_type = 1;
            
            //apabila commandnya tidak sesuai maka outputnya harus "UNKNOWN COMMAND"
            strcpy(data.msg_text, "UNKNOWN COMMAND");
            msgsnd(msg_id, (void*) &data, 500, 0);
        }

    }


Kemudian selanjutnya, terdapat looping yang berjalan terus-menerus. Namun, hanya dua user yang memiliki akses untuk mengirimkan perintah yang akan dibaca oleh stream.c. Agar dapat membedakan antara satu proses dengan proses lainnya, digunakan fungsi getpid().

**Perintah DECRYPT, kita harus ubah 3 metode dari ROT13, HEX, dan Base64.**

char *rot13(char *src){

    if(src == NULL) return NULL;

    char* result = malloc(strlen(src) + 1);

    if(result != NULL){
        strcpy(result, src);

        char* current_char = result;

        while(*current_char != '\0'){

            if((*current_char >= 97 && *current_char <= 122) || (*current_char >= 65 && *current_char <= 90)){

                if(*current_char > 109 || (*current_char > 77 && *current_char < 91)){

                    *current_char -= 13;
                }
                else{
                    *current_char += 13;
                }
            }
            current_char++;
        }
    }

    return result;
}

Untuk ROT13, saya mengambil referensi dari google. Source: https://gist.github.com/Ztuu/e9106e9095422a7d7266653f1e156366


char* hexToASCII(const char* hex) {
    size_t len = strlen(hex);
    if (len % 2 != 0) return NULL; // Return NULL jika jumlah digit hex bukan genap

    size_t output_len = len / 2;
    char* output = malloc(output_len + 1);
    if (output == NULL) return NULL; // Return NULL jika alokasi memori gagal

    for (size_t i = 0; i < output_len; i++) {
        char byte[3] = {hex[i * 2], hex[i * 2 + 1], '\0'}; // Ambil 2 digit hex untuk setiap byte ASCII
        output[i] = (char)strtol(byte, NULL, 16); // Convert hex menjadi bilangan desimal
    }
    output[output_len] = '\0'; // Tambahkan null terminator pada akhir string ASCII

    return output;
}

Untuk Hex, saya mengambil referensi dari google. Source: https://www.geeksforgeeks.org/convert-hexadecimal-value-string-ascii-value-string/

static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};
 
void build_decoding_table() {
 
    decoding_table = malloc(256);
 
    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char) encoding_table[i]] = i;
}

unsigned char *base64(const char *data,
                             size_t input_length,
                             size_t *output_length) {
 
    if (decoding_table == NULL) build_decoding_table();
 
    if (input_length % 4 != 0) return NULL;
 
    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;
 
    unsigned char *decoded_data = malloc(*output_length);
    unsigned char *hasil_decode = malloc(*output_length);

    if (decoded_data == NULL) return NULL;
 
    for (int i = 0, j = 0; i < input_length;) {
 
        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
 
        uint32_t triple = (sextet_a << 3 * 6)
        + (sextet_b << 2 * 6)
        + (sextet_c << 1 * 6)
        + (sextet_d << 0 * 6);
 
        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    for (int i = 0; i < *output_length; i++){

        if(decoded_data[i] != 0 && decoded_data[i] != 127){
            hasil_decode[i] = decoded_data[i];
        }
        else break;
    }
 
    return hasil_decode;
}

Untuk Base64, saya mengambil referensi dari google. Source: https://www.mycplus.com/source-code/c-source-code/base64-encode-decode/

**Setelah itu, kita mengurus hasil yang diterima dari message queue yang memiliki ID 12. Sebelumnya, karena ada pembatasan akses hanya untuk dua user, kita membuat sebuah struct yang digunakan untuk mengelola PID.**

![image](/uploads/fe9f142891d60040490a5900e31f63ed/image.png)

**Lalu kita inisialisasi semaphore**

![image](/uploads/84fe3aeed882cf6b3275ec1846f8f882/image.png)


**Inisialisasi juga shared memory**

![image](/uploads/15136ae85e0ad8da85a87c1472f66f64/image.png)


**Kemudian, mengambil dua data untuk mengurus 2 user saja.**

    while(running){

        // sem_destroy(&semaphore);
        // shmdt(pid_data);
        // shmctl(shem_id, IPC_RMID, NULL);

        msgrcv(msg_id, (void*) &data, BUFSIZ, msg_rcv, 0);

        // Akses shared memory dan semaphore
        sem_wait(&semaphore); // Mengurangi nilai semaphore menjadi 0

        // Menghandle kondisi jika pid1 masih bernilai 0 dan nilai pid dari messagequeue tidak sama dengan pid2. Atau kondisi dimana pid dari messagequeue sama dengan pid1. 
        if ((pid_data->pid1 == 0 && data.id != pid_data->pid2) || data.id == pid_data->pid1) {
            pid_data->pid1 = data.id;
            
            //bila printah merupakan "end", maka user tersebut tidak dapat mengirim perintah lagi
            if(strncmp(data.msg_text, "end", 3) == 0){
                printf("Process with PID %d is end.\n", data.id);

                printf("Data received: %s\n", data.msg_text);

                pid_data->pid1 = 0;

                flag = 0;

                sem_post(&semaphore); // Meningkatkan nilai semaphore kembali menjadi 1

            }
            
            else{
                printf("Process with PID %d is running.\n", data.id);
                flag = 1;
                sem_post(&semaphore);
            } 
        } 

        // Menghandle kondisi jika pid2 masih bernilai 0 dan nilai pid dari messagequeue tidak sama dengan pid1. Atau kondisi dimana pid dari messagequeue sama dengan pid2. 
        else if ((pid_data->pid2 == 0 && data.id != pid_data->pid1) || data.id == pid_data->pid2) {
            pid_data->pid2 = data.id;
            
            //bila printah merupakan "end", maka user tersebut tidak dapat mengirim perintah lagi
            if(strncmp(data.msg_text, "end", 3) == 0){

                printf("Process with PID %d is end.\n", data.id);
                printf("Data received: %s\n", data.msg_text);

                pid_data->pid2 = 0;

                flag = 0;

                sem_post(&semaphore); // Meningkatkan nilai semaphore kembali menjadi 1

            }

            else {
                printf("Process with PID %d is running.\n", data.id);
                flag = 1;
                sem_post(&semaphore);
            }
        } 
        
        //handle apabila pid1 dan pid2 telah penuh maka terjadi overload. 
        else{

            printf("Process with PID %d is unacceptable.\n", data.id);
            puts("STREAM SYSTEM OVERLOAD");
            flag = 0;
            sem_post(&semaphore);
        }

        sem_wait(&semaphore);
        ...


**Jika flag == 1, akan terjadi proses pemanggilan perintahnya (bukan perintah end)**

if(flag == 1){
    
    printf("Data received: %s with id: %d\n", data.msg_text, data.id);

    //jika data message = DECRYPT, maka memanggil fungsi DECRYPT()
    if(strncmp(data.msg_text, "DECRYPT", 7) == 0){

        DECRYPT();
        puts("");
    }

    //jika data message = LIST, maka memanggil fungsi LIST()
    else if(strncmp(data.msg_text, "LIST", 4) == 0){

        LIST();
        puts("");
    }

    //jika data message = PLAY, maka memanggil fungsi PLAY()
    else if(strncmp(data.msg_text, "PLAY", 4) == 0){

        //karena inputan user dari perintah play adalah 
        //contoh: PLAY "Flo"
        // Kita ingin mengambil kalimat Flo saja. 

        //Mencari karakter pertama dari tanda kutip
        char *ptr1 = strchr(data.msg_text, '\"');
        char *ptr2;

        char lagu_input[200] = {0};
        int user_id = -1;

        if(ptr1 != NULL){

            //pointer menunjuk ke karakter setelah tanda kutip
            ptr1++;

            //Mencari karakter kedua dari tanda kutip 
            ptr2 = strchr(ptr1, '\"');

            //Copy substring antara ptr1 dengan ptr2 ke array lagu_input dengan panjang substring dihitung dengan mengurangi ptr1 dari ptr2
            if(ptr2 != NULL) strncpy(lagu_input, ptr1, ptr2 - ptr1);
        }

        PLAY(data.id, lagu_input);
    }
    
    //jika data message = ADD, maka memanggil fungsi ADD()
    else if(strncmp(data.msg_text, "ADD", 3) == 0){

                //karena inputan user dari perintah ADD adalah 
        //contoh: ADD Flower - Jisoo
        // Kita ingin mengambil kalimat Flower - Jisoo saja. 

        //mencari substring dari ADD 
        char *ptr1 = strstr(data.msg_text, "ADD ");
        if (ptr1 != NULL) {
            ptr1 += 4; // maju sebanyak 4 karakter untuk melewati "CEK "
            char *ptr2 = strchr(ptr1, '\0'); // cari akhir string

            // menghapus whitespace di awal string
            while (*ptr1 == ' ') ptr1++;
            
            // menghapus whitespace di akhir string
            while (*(ptr2 - 1) == ' ') ptr2--;

            // menghitung panjang kalimat yang diambil
            size_t len = ptr2 - ptr1;

            // menyalin kalimat ke buffer
            char lagu_input[200];
            strncpy(lagu_input, ptr1, len);
            lagu_input[len] = '\0';

            ADD(data.id, lagu_input);
        }
    }
}

**Memanggil fungsi DECRYPT**

void DECRYPT(){

    char* json_data;
    char *filename = "song-playlist.json";
    FILE *file = fopen(filename, "r");

    if(file == NULL){
        puts("Gagal membuka file");
        exit(1);
    }

    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    rewind(file);

    json_data = (char*) malloc(file_size);

    fread(json_data, file_size, 1, file);

    fclose(file);

    char *json_start = strchr(json_data, '[');
    char *json_end = strchr(json_data, ']');

    char *method_start = strstr(json_start, "\"method\"");

    FILE *fp = fopen("playlist.txt", "w");

    while(method_start && method_start < json_end) {

        // Mengambil nilai method
        char *method_value_start = strchr(method_start, ':');
        char *method_value_end = strchr(method_value_start, ',');
        int method_value_len = method_value_end - method_value_start - 3;

        char method[method_value_len + 1];
        strncpy(method, method_value_start + 3, method_value_len);
        method[method_value_len] = '\0';

        strtok(method, "\"");

        printf("Method: %s\n", method);

        // Mengambil nilai song
        char *song_start = strstr(method_value_end, "\"song\"");
        char *song_value_start = strchr(song_start, ':');
        char *song_value_end = strchr(song_value_start, '}');
        long song_value_len = song_value_end - song_value_start - 3;

        char song[song_value_len + 1];
        strncpy(song, song_value_start + 3, song_value_len);
        song[song_value_len] = '\0';

        strtok(song, "\"");

        printf("Song: %s\n", song);
        
        //mulai decrypt:
        if(strcmp(method, "rot13") == 0){
            puts("INI ROT13");

            char *result = rot13(song);
            printf("Hasil decrypt dari rot13: %s\n",result);

            fprintf(fp, "%s\n", result);
        } 

        if(strcmp(method, "base64") == 0){

            char* hasil_decode = base64(song, song_value_len, &song_value_len);

            printf("Hasil decrypt dari base64: %s\n", hasil_decode);

            fprintf(fp, "%s\n", hasil_decode);
        } 

        if(strcmp(method, "hex") == 0){

            char* hexString = hexToASCII(song);

            printf("Hasil hex to string: %s\n", hexString);

            fprintf(fp, "%s\n", hexString);

        } 

        // Pindah ke objek method dan song berikutnya
        method_start = strstr(song_value_end, "\"method\"");
    }

    fclose(fp);
}


**Memanggil fungsi LIST**

// Fungsi untuk membandingkan dua string
int compare(const void* a, const void* b) {
    char* str1 = *(char**)a;
    char* str2 = *(char**)b;

    return strcmp(str1, str2);
}

void LIST() {

    FILE *file_song = fopen("playlist.txt", "r");
    if (file_song == NULL) {
        puts("File tidak ditemukan!");
    }

    char line[105];
    char lines[1005][105];
    char* uppercaseLines[1005];
    char* lowercaseLines[1005];
    char* numberLines[1005];
    int numUppercaseLines = 0;
    int numLowercaseLines = 0;
    int numNumberLines = 0;

    // Baca setiap baris dan cek huruf pertamanya
    for(int i = 0; i < 1005 && fgets(line, 105, file_song); i++) {
        
        //cek huruf besar
        if (line[0] >= 65 && line[0] <= 90) {
            // Alokasikan memori dan salin isi baris ke array untuk huruf besar
            uppercaseLines[numUppercaseLines] = lines[i];
            strcpy(uppercaseLines[numUppercaseLines], line);
            numUppercaseLines++;
        } 
        //cek huruf kecil
        else if (line[0] >= 97 && line[0] <= 122) {
            // Alokasikan memori dan salin isi baris ke array untuk huruf kecil
            lowercaseLines[numLowercaseLines] = lines[i];
            strcpy(lowercaseLines[numLowercaseLines], line);
            numLowercaseLines++;
        } 
        //cek angka dsb
        else{
            // Alokasikan memori dan salin isi baris ke array untuk angka
            numberLines[numNumberLines] = lines[i];
            strcpy(numberLines[numNumberLines], line);
            numNumberLines++;
        }
    }

    FILE *sort_file = fopen("sortingSong.txt", "w");
    
    //sorting 
    qsort(uppercaseLines, numUppercaseLines, sizeof(char*), compare);
    qsort(lowercaseLines, numLowercaseLines, sizeof(char*), compare);
    qsort(numberLines, numNumberLines, sizeof(char*), compare);

    //print untuk urutan angka dulu
    for(int i = 0; i < numNumberLines; i++){
        fputs(numberLines[i], sort_file);
        printf("%s\n", numberLines[i]);
    }
    //print untuk huruf kecil
    for(int i = 0; i < numLowercaseLines; i++){
        fputs(lowercaseLines[i], sort_file);
        printf("%s\n", lowercaseLines[i]);
    }
    //print untuk huruf besar
    for(int i = 0; i < numUppercaseLines; i++){
        fputs(uppercaseLines[i], sort_file);
        printf("%s\n", uppercaseLines[i]);
    }

    fclose(sort_file);

}

**Memanggil fungsi Play()**

void PLAY(const int x, const char* words){

    FILE * cek_song;
    char line_cek[150];
    char matchedLines[100][150];
    long numMatched = 0;

    cek_song = fopen("playlist.txt", "r");

    //ambil lagu di playlist.txt
    while(fgets(line_cek, 150, cek_song)){

        //diubah ke huruf kecil agar gampang
        char* tmp = strdup(line_cek);

        for(int i = 0; i < strlen(tmp); i++){
            tmp[i] = tolower(tmp[i]);
        }

        //diubah ke huruf kecil untuk kalimat dari user
        char* wordLower = strdup(words);

        for(int i = 0; i < strlen(wordLower); i++){
            wordLower[i] = tolower(wordLower[i]);
        }

        //melakukan cek dengan numMatched
        if(strstr(tmp, wordLower)){
            
            strcpy(matchedLines[numMatched], line_cek);
            numMatched++;
        }
    }

    fclose(cek_song);

    //Maka hanya terdapat 1 lagu, sehingga: 
    if(numMatched == 1) printf("USER %d PLAYING \"%s\"\n", x, matchedLines[0]);
    //Terdapat lebih dari 1 lagu yang memiliki kata yang mirip, sehingga:
    else if(numMatched > 1){
        printf("THERE ARE %ld SONG CONTAINING \"%s\":", numMatched, words);
        puts("");
        
        for(long i = 0; i < numMatched; i++){
            printf("%ld. %s", i + 1, matchedLines[i]);
        }
    }

    //apabila tidak ada lagunya, maka:
    else printf("THERE IS NO SONG CONTAINING \"%s\"\n", words);

}


**Memanggil fungsi ADD()**

void ADD(const int x, const char* newSong){

    FILE *add_song;
    add_song = fopen("playlist.txt", "r");

    char line[150];
    int flag = 0;
    while(fgets(line, 150, add_song)){
        
        //cek apakah ada kalimat yang mirip
        if(strstr(line, newSong)){
            flag = 1;
            break;
        }
    }

    fclose(add_song);

    if(flag == 0){
        add_song = fopen("playlist.txt", "a");

        fputs(newSong, add_song);

        fclose(add_song);

        printf("USER %d ADD %s\n", x, newSong);
    }
    // lagu sudah ada di playlist
    else puts("SONG ALREADY ON PLAYLIST");
}



#### Output Question 3
**Output fungsi DECRYPT**
![WhatsApp_Image_2023-05-13_at_20.46.57](/uploads/8fae0cefc8b38272576f09925da00dfa/WhatsApp_Image_2023-05-13_at_20.46.57.jpeg)

**Output fungsi LIST**
![WhatsApp_Image_2023-05-13_at_20.46.58](/uploads/9d49238ece68c8ec671286a7b2ca6e5e/WhatsApp_Image_2023-05-13_at_20.46.58.jpeg)

**Output fungsi ADD()**
![WhatsApp_Image_2023-05-13_at_20.46.59](/uploads/f62d6425a4b4c9e2163c19e11067c59d/WhatsApp_Image_2023-05-13_at_20.46.59.jpeg)

**Output fungsi Play()**
![WhatsApp_Image_2023-05-13_at_20.46.59__1_](/uploads/124cafbbc4f7771ea013801971a6ddc6/WhatsApp_Image_2023-05-13_at_20.46.59__1_.jpeg)

**Output end**
![WhatsApp_Image_2023-05-13_at_20.46.59__1_](/uploads/124cafbbc4f7771ea013801971a6ddc6/WhatsApp_Image_2023-05-13_at_20.46.59__1_.jpeg)

## Question 4
Suatu hari, Amin, seorang mahasiswa Informatika mendapati suatu file bernama hehe.zip. Di dalam file .zip tersebut, terdapat sebuah folder bernama files dan file .txt bernama extensions.txt dan max.txt. Setelah melamun beberapa saat, Amin mendapatkan ide untuk merepotkan kalian dengan file tersebut! 

#### 4A
**_SOAL :_**
Download dan unzip file tersebut dalam kode c bernama unzip.c.

**_Pengaplikasian Pada Program:_** 
```
#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<dirent.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<sys/msg.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<sys/sem.h>

pthread_t tid[2];
pid_t child;
int stats = 0;

void* dl(void *arg)
{
    char url[100];
    sprintf(url, "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download");
    char *args[] = {"wget", "-O", "hehe.zip", url, NULL};

    pthread_t id=pthread_self();
	int status;

	if(pthread_equal(id,tid[0])) //thread untuk download hehe.zip
	{
        child = fork();
		if (child==0) {
		    execv("/usr/bin/wget", args);
	    }
        else {
		    ((wait(&status)) > 0);
        }
    }
}

void* unzipping(void *arg)
{
    while(stats != 1){

    }

    char *args[] = {"unzip", "hehe.zip", NULL};
	
	pthread_t id=pthread_self();
	int status;

	if(pthread_equal(id,tid[0])) //thread untuk unzip hehe.zip
	{
		child = fork();
		if (child==0) {
            execv("/usr/bin/unzip", args);
	    }
        else {
		    wait(&status);
        }
    }

	return NULL;
}

int main(){
    int i=0;
	int err;
    while(i<1)
	{
        err=pthread_create(&(tid[i]),NULL,&dl,NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		i++;
	}
    pthread_join(tid[0],NULL);

    i=0;

	while(i<1)
	{
        err=pthread_create(&(tid[i]),NULL,&unzipping,NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		i++;
	}
	pthread_join(tid[0],NULL);

    return 0;
}
```

**_Penjelasan Kode:_**  
Pada soal ini akan dibuat file ``unzip.c`` yang terdapat implementasi multi-threading menggunakan pthread. Program ini memiliki dua thread yang bekerja secara paralel untuk melakukan pengunduhan dan ekstraksi file zip.

Pada thread pertama, yang diberi nama ``dl``, dilakukan pengunduhan file zip dari URL tertentu menggunakan perintah ``wget`` melalui ``execv``. Thread ini menggunakan fungsi ``pthread_create`` untuk membuat thread baru. Setelah pengunduhan selesai, status ``stats`` diubah menjadi 1.

Pada thread kedua, yang diberi nama ``unzipping``, dilakukan ekstraksi file zip menggunakan perintah ``unzip`` melalui ``execv``. Sebelum melakukan ekstraksi, thread ini menunggu hingga status ``stats`` berubah menjadi 1, yang menandakan bahwa pengunduhan file zip telah selesai. Setelah ekstraksi selesai, thread ini juga menggunakan fungsi ``pthread_create`` untuk membuat thread baru.

Di dalam fungsi ``main``, terdapat inisialisasi thread, pemanggilan fungsi ``pthread_create``, dan pemanggilan ``pthread_join`` untuk menunggu thread selesai. Setelah semua thread selesai, program akan mengembalikan nilai 0 dan berakhir. 

#### 4B, 4C, 4D, 4E
**_SOAL :_**      
Buat program categorize.c untuk mengumpulkan (move / copy) file sesuai extension-nya. Extension yang ingin dikumpulkan terdapat dalam file extensions.txt. Buatlah folder categorized dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua lowercase. File lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder other.
Pada file max.txt, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder other. Sehingga, jika penuh, buatlah folder baru dengan format extension (2), extension (3), dan seterusnya.

Output-kan pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut.
```
extension_a : banyak_file
extension_b : banyak_file
extension_c : banyak_file
other : banyak_file
```

Setiap pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading. Jika tidak menggunakan akan ada pengurangan nilai.

Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c buatlah log dengan format sebagai berikut.
DD-MM-YYYY HH:MM:SS ACCESSED [folder path]
DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]
DD-MM-YYYY HH:MM:SS MADE [folder name]
examples : 
02-05-2023 10:01:02 ACCESSED files
02-05-2023 10:01:03 ACCESSED files/abcd
02-05-2023 10:01:04 MADE categorized
02-05-2023 10:01:05 MADE categorized/jpg
02-05-2023 10:01:06 MOVED jpg file : files/abcd/foto.jpg > categorized/jpg
Catatan:
- Path dimulai dari folder files atau categorized
- Simpan di dalam log.txt
- ACCESSED merupakan folder files beserta dalamnya
- Urutan log tidak harus sama


**_Pengaplikasian Pada Program:_**    
```
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <pthread.h>
#include <ctype.h>
#include <math.h> 

#define MAX_LEN 256
#define MAX_PATH 256
#define MAX_EXT 5
#define MAX_DEST_SIZE 475 // maximum size of destination buffer

pthread_mutex_t lock;
FILE* log_file;

struct ThreadArgs {
    char dirname[MAX_LEN];
    int maxFile;
};

void writeLogMade(char createdDir[]){
    // menulis pesan log
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char logMADE[256] = "MADE ";
    char _time_str[80];
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t);

    strcat(logMADE, createdDir);
    pthread_mutex_lock(&lock);
    fprintf(log_file, "%s %s\n", _time_str, logMADE);
    pthread_mutex_unlock(&lock);
}

void writeLogAccess(char folderPath[]){
    // menulis pesan log
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char logAccess[256] = "ACCESSED ";
    char _time_str[80];
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t);

    strcat(logAccess, folderPath);
    pthread_mutex_lock(&lock);
    fprintf(log_file, "%s %s\n", _time_str, logAccess);
    pthread_mutex_unlock(&lock);
}


void* create_directory(void* arg) {
    struct ThreadArgs* thread_args = (struct ThreadArgs*) arg;
    char* dirname = thread_args->dirname;
    int maxFile = thread_args->maxFile;
    char ext[4];
    strcpy(ext, dirname);
    char categorized_dirname[MAX_LEN] = "categorized/";
    char new_dirname[MAX_LEN];
    struct stat st;

    // membuat nama direktori baru dengan menambahkan prefix "categorized/"
    strcpy(new_dirname, categorized_dirname);
    strcat(new_dirname, dirname);

    //hitung banyak file
    char countFile[256];
    char hehe_dir[20] ="files";
    char cmd[256];
    snprintf(cmd, sizeof(cmd), "find %s -type f -iname \"*.%s\" | wc -l", hehe_dir, ext);

    // Mengeksekusi command dan membuka stream untuk membaca outputnya
    FILE* fp;
    fp = popen(cmd, "r");
    if (fp == NULL) {
        printf("Error: Failed to execute command.\n");
    }

    // Membaca output dari stream dan menampilkannya ke layar
    writeLogAccess(hehe_dir);
    fgets(countFile, sizeof(countFile), fp);
    int numFiles = atoi(countFile);

    // ascending output
    char _write[MAX_LEN];
    system("touch buffer.txt");
    sprintf(_write, "echo \"extension_%s : %d\" >> buffer.txt", ext, numFiles);
    system(_write);
    
    //menghitung total direktori yang dibuat per ext 
    int numDir;
    if (maxFile == 0){
        numDir = 1;
    } else {
        numDir = numFiles / maxFile ;
        if (numFiles % maxFile > 0)
            numDir = numDir + 1;
    }

    // Menutup stream dan mengembalikan nilai 0 (sukses)
    pclose(fp);

    //create dir  if numDir == 0
    if (numDir == 0 && mkdir(new_dirname, 0777) == 0) {
        writeLogMade(new_dirname);
    }

    for (int i = 0; i < numDir; i++)
    {
        // mendefinisikan variable nama directory dengan suffix sesuai dengan index
        char _dirName[30];
        strcpy(_dirName, new_dirname);
        if(i > 0) {
            char suffix[10];
            sprintf(suffix, " (%d)", i+1);
            strcat(_dirName, suffix);
        }       

        // memeriksa apakah direktori sudah ada
        if (stat(_dirName, &st) == 0) {
            printf("Thread %ld: Direktori '%s' sudah ada.\n", pthread_self(), _dirName);
        } else {
                // membuat direktori jika belum ada
                if (mkdir(_dirName, 0777) == 0) {
                    writeLogMade(_dirName);
                    //move files according to their extensions
                    char command1[600];
                    char command2[500];
                    char command3[100];
                    char command4[600];
                    char logAccessSource[200];
                    char logAccessTarget[100];

                    sprintf(logAccessSource, "find %s/ -type f -iname \"*.%s\" |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt", hehe_dir, ext);
                    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", _dirName);
                    
                    system (logAccessSource);
                    system (logAccessTarget);

                    sprintf(command3, "mv \"{}\" \"%s/\"", _dirName);
                    sprintf(command2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED %s file : {} > %s\">> log.txt && %s", ext, _dirName, command3);

                    if (maxFile == 0)
                    {
                        sprintf(command4, "find %s/ -type f -iname \"*.%s\" |xargs -I {} sh -c '%s' ", hehe_dir, ext, command2);
                        system(command4);
                    } else {
                        sprintf(command1, "find %s/ -type f -iname \"*.%s\" |head -%d |xargs -I {} sh -c '%s' ", hehe_dir, ext, maxFile, command2);

                        system(command1);

                    }


                } else {
                    perror("Gagal membuat direktori");
                    
                }
            }
    }
    return NULL;
}


int main() {
    // membuka file log.txt
    log_file = fopen("log.txt", "a");
    if (log_file == NULL) {
        perror("Failed to open log file");
        exit(1);
    }

    // inisialisasi mutex lock, sinkronisasi akses ke bagian kritis dalam program yang dijalankan oleh beberapa thread.
    if (pthread_mutex_init(&lock, NULL) != 0) {
        perror("Failed to initialize mutex lock");
        exit(1);
    }

    char filename[50] = "extensions.txt";
    char dirname[MAX_LEN];
    FILE *fp;
    struct stat st;
    pthread_t tid[MAX_LEN];
    struct ThreadArgs thread_args[MAX_LEN];
    int i = 0;
    
    // membuat direktori "categorized" jika belum ada
    if (stat("categorized", &st) != 0) {
        mkdir("categorized", 0777);
        writeLogMade("categorized");
    }

    //membaca file max.txt dan menyimpan nilainya
    char maxfilename[50] = "max.txt";
    //open max.txt
    FILE *max;
    // membuka file untuk dibaca
    max = fopen(maxfilename, "r");

    // memeriksa apakah file berhasil dibuka
    if (max == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    int maxFile;
    fscanf(max, "%d", &maxFile); // read the first line of max.txt into firstLine

     // menutup file max.txt
    fclose(max);

    // membuka file extensions.txt untuk dibaca
    fp = fopen(filename, "r");

    // memeriksa apakah file extensions.txt berhasil dibuka
    if (fp == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    // membaca nama direktori dari file, satu per satu
    while (fgets(dirname, MAX_LEN, fp)) {
        // menghilangkan karakter newline di akhir string
        dirname[strcspn(dirname, "\n")] = 0;

        // menghapus spasi di akhir string
        size_t len = strlen(dirname);
        while (len > 0 && isspace(dirname[len - 1])) {
            len--;
        }
        dirname[len] = '\0';

        // membuat direktori lain secara asynchronous dengan menggunakan thread
        strcpy(thread_args[i].dirname, dirname);
        thread_args[i].maxFile = maxFile;
        pthread_create(&tid[i], NULL, create_directory, &thread_args[i]);
        i++;
    }

    // join thread yang masih berjalan
    for (int j = 0; j < i; j++) {
        pthread_join(tid[j], NULL);
    }

    // membuat direktori "other" di dalam "categorized" jika belum ada
    if (stat("categorized/other", &st) != 0) {
        writeLogMade("categorized/other");
        if (mkdir("categorized/other", 0777) != 0)
            printf("Gagal membuat direktori 'other' di dalam 'categorized'.\n");
    }

    // move other files to categorized/other
    char cmdOther1[600];
    char cmdOther2[500];
    char cmdOther3[100];
    char otherPath[20] = "categorized/other";
    char logAccessSource[200];
    char logAccessTarget[100];

    sprintf(logAccessSource, "find files/ -type f |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt");
    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", otherPath);
    
    system(logAccessSource);
    system(logAccessTarget);

    sprintf(cmdOther3, "mv '\\''{}'\\'' \"%s/\"", otherPath);
    sprintf(cmdOther2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED other file : '\\''{}'\\'' > %s\">> log.txt && %s", otherPath, cmdOther3);
    sprintf(cmdOther1, "find files/ -type f |xargs -I {} sh -c '%s' ", cmdOther2);
    system(cmdOther1);

    //output sort buffer.txt untuk jumlah file tiap extension
    system("sort buffer.txt");
    system("rm -rf buffer.txt");

    // count how many files in dir categorized/other
    DIR *dir;
    struct dirent *ent;
    int count = 0;
    if ((dir = opendir (otherPath)) != NULL) {
        writeLogAccess(otherPath);
        while ((ent = readdir (dir)) != NULL) {
            if(ent->d_type == DT_REG) {
                count++;
            }
        }
        closedir (dir);
    } else {
        perror ("Tidak bisa membuka direktori");
        return 1;
    }
    printf("other : %d\n", count);

    // menutup file
    fclose(fp);
    
   // menghancurkan mutex lock
    pthread_mutex_destroy(&lock);

    // menutup file log
    fclose(log_file);

    return 0;
}

```
   
**_Penjelasan Kode:_**   
File ``categorize.c`` memiliki beberapa fungsi dan proses untuk mengategorikan file berdasarkan ekstensinya ke dalam folder-folder yang sesuai. Program ini juga mencatat log setiap kali terjadi pengaksesan folder, pemindahan file, atau pembuatan folder.

Pertama, program membuka file log.txt dengan mode "a" untuk menambahkan pesan log ke dalam file tersebut. Jika file tidak dapat dibuka, maka akan muncul pesan error dan program akan keluar.

Selanjutnya, program melakukan inisialisasi mutex lock menggunakan fungsi pthread_mutex_init(). Mutex lock digunakan untuk melakukan sinkronisasi akses ke bagian kritis dalam program yang dijalankan oleh beberapa thread. Jika inisialisasi mutex lock gagal, program akan mencetak pesan error dan keluar.

Lalu, program membaca file extensions.txt yang berisi daftar ekstensi file. Setiap nama direktori dibaca satu per satu, kemudian dibuat thread baru menggunakan fungsi pthread_create(). Setiap thread akan menjalankan fungsi create_directory() yang mengkategorikan file berdasarkan ekstensi ke dalam folder yang sesuai. Thread akan bergabung kembali dengan pemanggilnya menggunakan fungsi pthread_join().

Akan dilakukan pengecekan dan pembuatan direktori "categorized/other" jika belum ada. Kemudian, file yang tidak memiliki ekstensi yang terdaftar akan dipindahkan ke folder "categorized/other". Proses ini juga mencatat log untuk setiap pengaksesan folder dan pemindahan file yang dilakukan.

Program melakukan sorting terhadap file buffer.txt yang berisi jumlah file untuk setiap ekstensi. Hasil sorting tersebut ditampilkan sebagai output. Kemudian file buffer.txt dihapus.

Terakhir, program menghitung jumlah file yang ada dalam direktori "categorized/other" dan mencetak hasilnya. Program menutup file extensions.txt, menghancurkan mutex lock, menutup file log, dan mengembalikan nilai 0 sebagai tanda bahwa program telah selesai berjalan.

Dengan demikian, program ini mengimplementasikan mekanisme pengkategorian file dan pencatatan log setiap kali terjadi aksi yang relevan, seperti pengaksesan folder, pemindahan file, dan pembuatan folder.
 

#### 4F    
**_SOAL :_**  
Untuk mengecek apakah log-nya benar, buatlah suatu program baru dengan nama logchecker.c untuk mengekstrak informasi dari log.txt dengan ketentuan sebagai berikut.
- Untuk menghitung banyaknya ACCESSED yang dilakukan.
- Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.
- Untuk menghitung banyaknya total file tiap extension, terurut secara ascending.

**_Pengaplikasian Pada Program:_**  
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    // Accessed count
    printf ("\e[33mTotal banyak akses:\e[0m\n");
    system("grep ACCESSED log.txt | wc -l");

    // List folders & total file
    printf ("\e[33m\nList folder & total file tiap folder:\e[0m\n");
    system("grep 'MOVED\\|MADE' log.txt \
    | awk -F 'categorized' '{print \"categorized\"$NF}' \
    | sed '/^$/d' \
    | sort \
    | uniq -c \
    | awk '{print $2 \" \"$3 \"=\" \" \" $1-1}'");


    // total file tiap extension terurut secara ascending
    printf ("\n\e[33mTotal file tiap extension\e[0m\n");
    system("grep 'MADE\\|MOVED' log.txt \
    | grep -o 'categorized/[^o]*' \
    | sed 's/categorized\\///' \
    | sed '/^$/d' \
    | sed 's/ \\([^o]*\\)//' \
    | sort \
    | uniq -c \
    | awk '{print $2 \" \" \"=\" \" \" $1-1}'");

    return 0;
}

```

**_Penjelasan Kode:_**   
Pada soal ini akan dibuat file ``logchecker.c`` yang menggunakan fungsi ``printf`` dan ``system`` untuk melakukan analisis pada file ``log.txt`` yang dihasilkan oleh program ``categorize.c``.

Pertama, program mencetak jumlah total akses dengan menggunakan perintah system untuk menjalankan perintah shell. Perintah ``grep ACCESSED log.txt | wc -l`` digunakan untuk menghitung jumlah baris yang mengandung kata "ACCESSED" dalam file ``log.txt``, sehingga menghasilkan total banyak akses.

Selanjutnya, program mencetak list folder dan total file dalam setiap folder. Ini dilakukan dengan menggunakan perintah ``system`` yang kompleks. Pertama, perintah ``grep 'MOVED\\|MADE' log.txt`` digunakan untuk mencari baris-baris yang mengandung kata "MOVED" atau "MADE" dalam file ``log.txt``. Kemudian, dengan menggunakan perintah ``awk -F 'categorized' '{print \"categorized\"$NF}'``, program mengubah format output untuk mencetak hanya path folder setelah kata "categorized". Perintah ``sed '/^$/d'`` digunakan untuk menghapus baris kosong. Selanjutnya, perintah ``sort`` digunakan untuk mengurutkan output secara alfabetis. Setelah itu, perintah ``uniq -c`` digunakan untuk menghitung jumlah kemunculan setiap folder. Terakhir, perintah ``awk '{print $2 \" \"$3 \"=\" \" \" $1-1}'`` digunakan untuk mencetak folder path dan total file dalam setiap folder.

Terakhir, program mencetak total file tiap extension secara terurut secara ascending. Hal ini dilakukan dengan menggunakan perintah ``system`` yang kompleks. Pertama, perintah ``grep 'MADE\\|MOVED' log.txt`` digunakan untuk mencari baris-baris yang mengandung kata "MADE" atau "MOVED" dalam file ``log.txt``. Kemudian, dengan menggunakan perintah ``grep -o 'categorized/[^o]*'``, program mencari pola "categorized/" diikuti dengan karakter-karakter lain selain huruf "o". Perintah ``sed 's/categorized\\///'`` digunakan untuk menghapus kata "categorized/". Selanjutnya, perintah ``sed '/^$/d'`` digunakan untuk menghapus baris kosong. Setelah itu, perintah ``sed 's/ \\([^o]*\\)//'`` digunakan untuk menghapus karakter setelah spasi, yang merupakan bagian dari folder path. Setelah itu, perintah sort digunakan untuk mengurutkan output secara alfabetis. Terakhir, perintah ``uniq -c`` digunakan untuk menghitung jumlah kemunculan setiap extension. Terakhir, perintah ``awk '{print $2 \" \" \"=\" \" \" $1-1}'`` digunakan untuk mencetak extension dan total file dalam setiap extension.

Program kemudian mengembalikan nilai 0, menandakan bahwa program telah selesai dijalankan.

#### Output Question 4
**_unzip.c :_**

![unzip](/uploads/30ee7aa799e4b9953008be0db62de3e6/unzip.jpg)

**_categorize.c :_**

![categorize](/uploads/64d1f54001b7d3cf1b5677b7c1e232d2/categorize.jpg)

**_logchecker.c :_**

![logchecker](/uploads/b53804b900e0d9cdc33fcad62a8411c8/logchecker.jpg)
