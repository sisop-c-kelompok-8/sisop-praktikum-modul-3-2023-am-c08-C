#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/shm.h>

int main() {
    // Menginisialisasi fungsi random
    srand(time(NULL));

    // Matriks pertama (Ordo 4x2)
    int matriks1[4][2];
    printf("Matriks pertama:\n");
    for (int i = 0; i < 4; i++) {
        printf(" [ ");
        for (int j = 0; j < 2; j++) {
            matriks1[i][j] = rand() % 5 + 1; // nilai random dari 1-5
            printf("%d ", matriks1[i][j]);
        }
        printf("] \n");
    }

    // Matriks kedua (Ordo 2x5)
    int matriks2[2][5];
    printf("Matriks kedua:\n");
    for (int i = 0; i < 2; i++) {
        printf(" [ ");
        for (int j = 0; j < 5; j++) {
            matriks2[i][j] = rand() % 4 + 1; // nilai random dari 1-4
            printf("%d ", matriks2[i][j]);
        }
        printf("] \n");
    }

    // Membuat shared memory
    int shmid;
    key_t key = 1234;

    if ((shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

    // Perkalian matriks pertama dan matriks kedua (Ordo 4x5)
    int (*hasil)[4][5];
    int hasil_kali;
    hasil = shmat(shmid,NULL,0);//attach ke shared memory
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            // (*hasil)[i][j] = 0;
            hasil_kali = 0;
            for (int k = 0; k < 2; k++) {
                hasil_kali += matriks1[i][k] * matriks2[k][j];
            }
            (*hasil)[i][j]=hasil_kali;
        }
    }


    // Output hasil perkalian
    printf("Hasil perkalian:\n");
    for (int i = 0; i < 4; i++) {
        printf(" [ ");
        for (int j = 0; j < 5; j++) {
            printf("%d ", (*hasil)[i][j]);
        }
        printf("] \n");
    }

    // // Melepaskan shared memory
    // shmdt(shm);

    // // Menghapus shared memory
    // shmctl(shmid, IPC_RMID, NULL);

    return 0;}