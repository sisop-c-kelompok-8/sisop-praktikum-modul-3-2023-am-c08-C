#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <stdint.h>

#define ull unsigned long long

ull hasil_faktorial[4][5];
int (*shmarr)[4][5];

void faktorial(int n){
    for(int i = 0; i < 5; i++){
        ull hasil = 1;
        for(int j=1;j<=(*shmarr)[n][i];j++){
            hasil *= j;
        }
        hasil_faktorial[n][i]=hasil;
    }
}

int main() {
    clock_t start, finish;
    double cpu_time;
    start = clock();
    // Meng-attach shared memory dari program "kalian.c"
    int shmid;
    key_t key = 1234;

    if ((shmid = shmget(key, sizeof(int[4][5]), 0666)) < 0) {
        perror("shmget");
        exit(1);
    }
    (shmarr = shmat(shmid, NULL, 0)); //mengambil / attach dari shared memory
    

    // Menampilkan hasil perkalian matriks dari shared memory
    printf("Hasil perkalian:\n");
    for (int i = 0; i < 4; i++) {
        printf(" [ ");
        for (int j = 0; j < 5; j++) {
            printf("%d ", (*shmarr)[i][j]);
        }
        printf("] \n");
    }

    for (int i=0;i<4;i++){
        faktorial(i);
    }

    printf("Hasil faktorial:\n");
    
    for (int i = 0; i < 4; i++) {
        printf(" [ ");
        for (int j = 0; j < 5; j++) {
            printf("%llu ", hasil_faktorial[i][j]);
        }
        printf("] \n");
    }

    // // Melepaskan shared memory
    // shmdt(shm);
    finish = clock();

    // Calculate CPU time 
    cpu_time = ((double) (finish - start)) / CLOCKS_PER_SEC;

    // Print CPU time used
    printf("CPU time: %f s\n", cpu_time);

    return 0;
}