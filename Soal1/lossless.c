#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define READ_END 0
#define WRITE_END 1

// struktur node Huffman tree
typedef struct Node {
    char letter;
    int freq;
    struct Node *left;
    struct Node *right;
} Node;

// fungsi untuk membuat node baru
Node *create_node(char letter, int freq) {
    Node *new_node = (Node *)malloc(sizeof(Node));
    new_node->letter = letter;
    new_node->freq = freq;
    new_node->left = NULL;
    new_node->right = NULL;
    return new_node;
}

// fungsi untuk mengurutkan array node berdasarkan frekuensi
void insertion_sort(Node **array, int n) {
    int i, j;
    Node *key;
    for (i = 1; i < n; i++) {
        key = array[i];
        j = i - 1;
        while (j >= 0 && array[j]->freq > key->freq) {
            array[j + 1] = array[j];
            j--;
        }
        array[j + 1] = key;
    }
}

// fungsi untuk membangun Huffman tree
Node *build_huffman_tree(int *freq) {
    int i, j, n;
    n = 0;
    for (i = 0; i < 26; i++) {
        if (freq[i] > 0) {
            n++;
        }
    }
    Node **array = (Node **)malloc(n * sizeof(Node *));
    j = 0;
    for (i = 0; i < 26; i++) {
        if (freq[i] > 0) {
            array[j] = create_node(i + 'A', freq[i]);
            j++;
        }
    }
    while (n > 1) {
        insertion_sort(array, n);
        Node *new_node = create_node('\0', array[0]->freq + array[1]->freq);
        new_node->left = array[0];
        new_node->right = array[1];
        array[0] = new_node;
        for (i = 1; i < n - 1; i++) {
            array[i] = array[i + 1];
        }
        n--;
    }
    return array[0];
}

// fungsi untuk dekode kode Huffman
char decode_huffman(Node *root, char *code) {
    Node *curr = root;
    while (*code != '\0') {
        if (*code == '0') {
            curr = curr->left;
        } else {
            curr = curr->right;
        }
        code++;
    }
    return curr->letter;
}

// lakukan kompresi file dengan menggunakan algoritma Huffman
void compress_file(int *freq) {
    Node *root = build_huffman_tree(freq);

    // buat file terkompresi
    FILE *input_file = fopen("file.txt", "r");
    FILE *output_file = fopen("file_compressed.bin", "wb");
    if (input_file == NULL || output_file == NULL) {
        perror("File opening failed");
        return;
    }

    // simpan Huffman tree pada file terkompresi
    fwrite(freq, sizeof(int), 26, output_file);
    fwrite(&root->letter, sizeof(char), 1, output_file);

    // ubah karakter pada file menjadi kode Huffman dan kirim ke program
int ch;
while ((ch = fgetc(input_file)) != NULL) {
    if (ch >= 'A' && ch <= 'Z') {
        char code[27] = {0};
        int code_len = 0;
        Node *curr = root;
        while (curr->left != NULL || curr->right != NULL) {
            if (ch == curr->left->letter) {
                code[code_len++] = '0';
                curr = curr->left;
            } else {
                code[code_len++] = '1';
                curr = curr->right;
            }
        }
        code[code_len] = '\0';
        fwrite(code, sizeof(char), code_len, output_file);
    }
}

// tutup file
fclose(input_file);
fclose(output_file);
}

// dekompresi file dengan menggunakan algoritma Huffman
void decompress_file() {
// buka file terkompresi
FILE *compressed_file = fopen("file_compressed.bin", "rb");
if (compressed_file == NULL) {
perror("File opening failed");
return;
}
// baca frekuensi huruf dan bangun Huffman tree
int freq[26] = {0};
fread(freq, sizeof(int), 26, compressed_file);
Node *root = build_huffman_tree(freq);

// baca root letter dari file terkompresi
char root_letter;
fread(&root_letter, sizeof(char), 1, compressed_file);
fclose(compressed_file);

// dekompresi file dan hitung jumlah bit setelah dikompresi
int bit_before = 0;
int bit_after = 0;
FILE *input_file = fopen("file.txt", "r");
FILE *output_file = fopen("file_decompressed.txt", "w");
if (input_file == NULL || output_file == NULL) {
    perror("File opening failed");
    return;
}
while (( ch = fgetc(input_file)) != EOF) {
    if (ch >= 'A' && ch <= 'Z') {
        bit_before += 8;
        char code[27] = {0};
        int code_len = 0;
        while (ch != root_letter) {
            bit_before += 1;
            if (ch < root_letter) {
                code[code_len++] = '0';
                ch = fgetc(input_file);
            } else {
                code[code_len++] = '1';
                ch = fgetc(input_file);
            }
        }
        code[code_len] = '\0';
        bit_after += strlen(code);
        fprintf(output_file, "%c", decode_huffman(root, code));
    }
}

// tutup file
fclose(input_file);
fclose(output_file);

// tampilkan perbandingan jumlah bit sebelum dan setelah dikompresi
printf("Jumlah bit sebelum kompresi: %d\n", bit_before);
printf("Jumlah bit setelah kompresi: %d\n", bit_after);
}

int main() {
// buat pipe untuk mengirim hasil perhitungan frekuensi tiap huruf ke child process
int freq_pipe[2];
if (pipe(freq_pipe) == -1) {
perror("Pipe failed");
return 1;
}
// fork child process
pid_t pid = fork();
if (pid == -1) {
    perror("Fork failed");
    return 1;
} else if (pid == 0) {
    // child process
    close(freq_pipe[WRITE_END]);
    // baca frekuensi huruf yang dikirim oleh parent process
    int freq[26] = {0};
    close(freq_pipe[WRITE_END]);
    read(freq_pipe[READ_END], freq, sizeof(freq));
    close(freq_pipe[READ_END]);

    // bangun Huffman tree berdasarkan frekuensi huruf
    Node *root = build_huffman_tree(freq);

    // simpan Huffman tree pada file terkompresi
    FILE *compressed_file = fopen("file_compressed.bin", "wb");
    if (compressed_file == NULL) {
        perror("File opening failed");
        return 1;
    }
    fwrite(freq, sizeof(int), 26, compressed_file);
    fwrite(&root->letter, sizeof(char), 1, compressed_file);
    save_huffman_tree(root, compressed_file);
    fclose(compressed_file);

    // kirim kode Huffman tiap huruf ke parent process
    compress_file(root);

    return 0;
} else {
    // parent process
    // baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut
    count_frequency(freq_pipe);

    // tunggu child process selesai
    wait(NULL);

    // baca Huffman tree dari file terkompresi
    decompress_file();
}

return 0;
}


