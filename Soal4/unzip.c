#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<dirent.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<sys/msg.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<sys/sem.h>

pthread_t tid[2];
pid_t child;
int stats = 0;

void* dl(void *arg)
{
    char url[100];
    sprintf(url, "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download");
    char *args[] = {"wget", "-O", "hehe.zip", url, NULL};

    pthread_t id=pthread_self();
	int status;

	if(pthread_equal(id,tid[0])) //thread untuk download hehe.zip
	{
        child = fork();
		if (child==0) {
		    execv("/usr/bin/wget", args);
	    }
        else {
		    ((wait(&status)) > 0);
        }
    }

    stats = 1;

	return NULL;
}

void* unzipping(void *arg)
{
    while(stats != 1){

    }

    char *args[] = {"unzip", "hehe.zip", NULL};
	
	pthread_t id=pthread_self();
	int status;

	if(pthread_equal(id,tid[0])) //thread untuk unzip hehe.zip
	{
		child = fork();
		if (child==0) {
            execv("/usr/bin/unzip", args);
	    }
        else {
		    wait(&status);
        }
    }

	return NULL;
}

int main(){
    int i=0;
	int err;
    while(i<1)
	{
        err=pthread_create(&(tid[i]),NULL,&dl,NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		i++;
	}
    pthread_join(tid[0],NULL);

    i=0;

	while(i<1)
	{
        err=pthread_create(&(tid[i]),NULL,&unzipping,NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		i++;
	}
	pthread_join(tid[0],NULL);

    return 0;
}
